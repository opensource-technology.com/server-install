#02 00 * * * /bin/sh /mnt/nfs_share/imedx/log/backup/cron_log.sh

app1=imedx1
POD1=$(kubectl get pod -l name=$app1 -o jsonpath="{.items[0].metadata.name}")
BEGIN_DATE=$(date --date="yesterday" +%Y-%m-%d)
#kubectl exec -ti $POD1 -- uname -a
echo "copying log $POD1 to $app1"
kubectl cp $POD1:/app/iMedX/standalone/log/server.log.$BEGIN_DATE $app1/server.log.$BEGIN_DATE
tar zcf $app1/server.log.$BEGIN_DATE.tgz $app1/server.log.$BEGIN_DATE
rm -rf $app1/server.log.$BEGIN_DATE
kubectl exec -ti $POD1 -- rm -f iMedX/standalone/log/server.log.$BEGIN_DATE

app2=imedx2
POD2=$(kubectl get pod -l name=$app2 -o jsonpath="{.items[0].metadata.name}")
BEGIN_DATE=$(date --date="yesterday" +%Y-%m-%d)
#kubectl exec -ti $POD2 -- uname -a
echo "copying log $POD2 to $app2"
kubectl cp $POD2:/app/iMedX/standalone/log/server.log.$BEGIN_DATE $app2/server.log.$BEGIN_DATE
tar zcf $app2/server.log.$BEGIN_DATE.tgz $app2/server.log.$BEGIN_DATE
rm -rf $app2/server.log.$BEGIN_DATE
kubectl exec -ti $POD2 -- rm -f iMedX/standalone/log/server.log.$BEGIN_DATE

app3=imedx3
POD3=$(kubectl get pod -l name=$app3 -o jsonpath="{.items[0].metadata.name}")
BEGIN_DATE=$(date --date="yesterday" +%Y-%m-%d)
#kubectl exec -ti $POD3 -- uname -a
echo "copying log $POD3 to $app3"
kubectl cp $POD3:/app/iMedX/standalone/log/server.log.$BEGIN_DATE $app3/server.log.$BEGIN_DATE
tar zcf $app3/server.log.$BEGIN_DATE.tgz $app3/server.log.$BEGIN_DATE
rm -rf $app3/server.log.$BEGIN_DATE
kubectl exec -ti $POD3 -- rm -f iMedX/standalone/log/server.log.$BEGIN_DATE