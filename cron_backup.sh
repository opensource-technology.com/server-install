#!/bin/bash
# Script install for opt Ubuntu 16.04.2 amd64
# By nitichai May 26,2017 
# Change Backup to Parallel dumps PostgreSQL 10
instHos=/root/cron_pg_10
etpr=/etc/profile
pgHome=/opt/PostgreSQL/10/bin
hCro=/backup/cron
hImp=/backup/implement
cd $instHos
#DB
pgPath=$pgHome
echo -n "Please enter your database's name1 : ";read DB
echo -n "Please enter your database's name2 : ";read DB2
#$pgPath/createdb $DB -U postgres
##sleep 1

#Backup
mkdir /backup
mkdir /backup/postgres_backup
pgBak=/backup/postgres_backup
cd $pgBak
mkdir -p $hCro Daily Daily2 CheckDB Sun Mon Tue Wed Thu Fri Sat Month Midmonth
cd $hCro
echo "#!/bin/sh" > daily.sh
echo "/bin/rm -rf $pgBak/Daily/*" >> daily.sh
echo "$pgPath/pg_dump -p 5434 -Fd $DB -j10 -f $pgBak/Daily/$DB\$(date +%Y%m%d)" >> daily.sh
echo "$pgPath/pg_dump -p 5434 -Fd $DB2 -j10 -f $pgBak/Daily/$DB2\$(date +%Y%m%d)" >> daily.sh
cp daily.sh daily2.sh;sed -i "s/Daily/Daily2/g" daily2.sh
echo "#!/bin/sh" > check.sh
echo "du -sm --time $pgBak/Daily/* >> $pgBak/CheckDB/sum_db.txt" >> check.sh
echo "#!/bin/sh" > sun.sh
echo "/bin/rm -rf $pgBak/Sun/*" >> sun.sh
echo "$pgPath/pg_dump -p 5434 -Fd $DB -j10 -f $pgBak/Sun/$DB\$(date +%Y%m%d)" >> sun.sh
echo "$pgPath/pg_dump -p 5434 -Fd $DB2 -j10 -f $pgBak/Sun/$DB2\$(date +%Y%m%d)" >> sun.sh
cp sun.sh mon.sh;sed -i "s/Sun/Mon/g" mon.sh
cp sun.sh tue.sh;sed -i "s/Sun/Tue/g" tue.sh
cp sun.sh wed.sh;sed -i "s/Sun/Wed/g" wed.sh
cp sun.sh thu.sh;sed -i "s/Sun/Thu/g" thu.sh
cp sun.sh fri.sh;sed -i "s/Sun/Fri/g" fri.sh
cp sun.sh sat.sh;sed -i "s/Sun/Sat/g" sat.sh
cp sun.sh month.sh;sed -i "s/Sun/Month/g" month.sh
cp sun.sh midmonth.sh;sed -i "s/Sun/Midmonth/g" midmonth.sh
chmod 755 *

#Crontab
crontab -u postgres $instHos/cron_postgres
chown postgres.postgres -R $hCro $pgBak
clear;echo
echo "  Backup crontab files are in : $hCro"
echo "  Database backup files are in : $pgBak"