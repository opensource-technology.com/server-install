#!/bin/bash
xset s noblank
xset s off
xset -dpms

unclutter -idle 0.5 -root &

sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' /home/qdisplay/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' /home/qdisplay/.config/chromium/Default/Preferences

#/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk --incognito --disable-translate http://ismart.hospital-os.com/queue/pharmacy?lang=th
/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk --incognito --disable-translate --password-store=basic https://bit.ly/2sAHUwp