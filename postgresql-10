#!/bin/bash
#
# chkconfig: 2345 85 15
# description: Starts and stops the PostgreSQL 10 database server

# Source function library.
if [ -f /etc/rc.d/functions ];
then
    . /etc/init.d/functions
fi

NAME=postgresql-10
LOCKFILE=/var/lock/subsys/$NAME

# PostgreSQL Service script for Linux

start()
{
	su - postgres -c "touch /imeddb/data/log/startup.log"
	echo $"Starting PostgreSQL 10: "

	su -s /bin/sh - postgres -c "/opt/PostgreSQL/10/bin/pg_ctl -w start -D \"/imeddb/data\" -l \"/imeddb/data/log/startup.log\""

	if [ $? -eq 0 ];
	then
			OS=`uname -n`
			if [ "$OS" != "ubuntu" ];
			then
			touch $LOCKFILE
			fi
			echo "PostgreSQL 10 started successfully"
			exit 0

	else
		echo "PostgreSQL 10 did not start in a timely fashion, please see /imeddb/data/log/startup.log for details"
                exit 1
	fi
}

stop()
{
	echo $"Stopping PostgreSQL 10: "
	su -s /bin/sh - postgres -c "/opt/PostgreSQL/10/bin/pg_ctl stop -m fast -w -D \"/imeddb/data\""
	if [ $? -eq 0 ];
	then
		rm -f $LOCKFILE
	fi
}

restart()
{
	su - postgres -c "touch /imeddb/data/log/startup.log"
	echo $"Restarting PostgreSQL 10: "

	su -s /bin/sh - postgres -c "/opt/PostgreSQL/10/bin/pg_ctl -w restart -D \"/imeddb/data\" -l \"/imeddb/data/log/startup.log\" -m fast"

	if [ $? -eq 0 ];
	then
			OS=`uname -n`
			if [ "$OS" != "ubuntu" ];
			then
			touch $LOCKFILE
			fi
		echo "PostgreSQL 10 restarted successfully"
                exit 0
	else
		echo "PostgreSQL 10 did not start in a timely fashion, please see /imeddb/data/log/startup.log for details"
                exit 1
	fi
}

reload()
{
	echo $"Reloading PostgreSQL 10: "
	su -s /bin/sh - postgres -c "/opt/PostgreSQL/10/bin/pg_ctl reload -D \"/imeddb/data\""
}

# See how we were called.
case "$1" in
  start)
        start
        ;;
  stop)
        stop
        ;;
  restart)
        restart
        ;;
  reload)
	reload
	;;
  condrestart)
        if [ -f "/imeddb/data/postmaster.pid" ]; then
            restart
        fi
        ;;
  status)
        su -s /bin/sh - postgres -c "/opt/PostgreSQL/10/bin/pg_ctl status -D \"/imeddb/data\""
        ;;
  *)
        echo $"Usage: /opt/PostgreSQL/10/installer/server/startupcfg.sh {start|stop|restart|condrestart|status}"
        exit 1
esac