#pod=$(kubectl get pods -o=name | grep imedx3) 
#echo "$pod"
#kubectl cp $pod:/app/iMedX/standalone/log $(date +%Y%m%d)/
app1=imedx1
POD1=$(kubectl get pod -l name=$app1 -o jsonpath="{.items[0].metadata.name}")
BEGIN_DATE=$(date --date="yesterday" +%Y-%m-%d)
#kubectl exec -ti $POD1 -- uname -a
echo "copying log $POD1 to $app1"
#kubectl cp $POD1:/app/iMedX/standalone/server.log.$BEGIN_DATE $app1/$(date +%Y%m%d)/
kubectl cp $POD1:/app/iMedX/standalone/log/server.log $app1.server.log
tar zcf $app1.server.log.tgz $app1.server.log
rm -rf $app1.server.log
#kubectl exec -ti $POD1 -- rm -f iMedX/standalone/log/server.log.$BEGIN_DATE

app2=imedx2
POD2=$(kubectl get pod -l name=$app2 -o jsonpath="{.items[0].metadata.name}")
#kubectl exec -ti $POD1 -- uname -a
echo "copying log $POD2 to $app2"
kubectl cp $POD2:/app/iMedX/standalone/log/server.log $app2.server.log
tar zcf $app2.server.log.tgz $app2.server.log
rm -rf $app2.server.log

app3=imedx3
POD3=$(kubectl get pod -l name=$app3 -o jsonpath="{.items[0].metadata.name}")
#kubectl exec -ti $POD3 -- uname -a
echo "copying log $POD3 to $app3"
kubectl cp $POD3:/app/iMedX/standalone/log/server.log $app3.server.log
tar zcf $app3.server.log.tgz $app3.server.log
rm -rf $app3.server.log